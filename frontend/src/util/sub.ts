import {useEffect, useState} from "react";

export class Sub<T> {
    _listeners: ((obj: T) => void)[] = [];
    _current: any = null;

    subscribe(listener: any) {
        if (this._listeners.indexOf(listener) < 0) {
            this._listeners.push(listener);
        }
    }

    unsubscribe(listener: any) {
        let index = this._listeners.indexOf(listener);
        if (index >= 0) {
            this._listeners.splice(index, 1);
        }
    }

    get() {
        return this._current;
    }

    set(obj: T) {
        this._current = obj;
        this._listeners.forEach(cb => cb(obj));
    }
}

export function useSub<T>(sub: Sub<T>, effectChanges: ((v: T) => any[]) | null = null): T {
    const [currentState, stateSetter] = useState(sub.get());

    useEffect(() => {
        let listener = (obj: T) => stateSetter(obj);
        sub.subscribe(listener);
        return () => sub.unsubscribe(listener);
    }, effectChanges ? effectChanges(currentState) : []);

    return currentState;
}

/**
 * Promisify emit.
 * @param event
 * @param arg
 */
export function emit(socket: SocketIOClient.Socket, event: string, arg: any = null) {
    return new Promise((resolve, reject) => {
        const cb = (err: any, res: any) => {
            if (err) {
                return reject(err);
            }

            resolve(res);
        };

        if (arg === null || typeof arg === 'undefined') {
            socket.emit(event, cb);
        } else {
            socket.emit(event, arg, cb);
        }

    })
}
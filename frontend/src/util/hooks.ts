import {useEffect, useState} from "react";

export function useUpdateAfterDelay(delay: number) {
    const [_, timedUpdateSet] = useState(0);
    useEffect(() => {
        let timeoutId = setTimeout(() => {
            timedUpdateSet(v => v + 1);
        }, delay);
        return () => clearTimeout(timeoutId)
    })
}

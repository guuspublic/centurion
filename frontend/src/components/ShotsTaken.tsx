import React from 'react';
import {Col, Progress} from "antd"
import {roomTime, useTimeline} from "../lib/Connection";
import {useUpdateAfterDelay} from "../util/hooks";


const ShotsTaken = () => {
    let timeline = useTimeline();

    useUpdateAfterDelay(1000);

    let taken = 0;

    if (timeline) {
        let [current, _] = timeline.eventAtTime(roomTime(), 'shot');
        if (current) {
            taken = current.shotCount!!;
        }
    }

    return (
        <>
            <h1>Shots genomen:</h1>
            <Progress type="circle"
                      percent={taken}
                      format={_ => taken + ' / 100'}
                      status="normal"
                      strokeColor={"#304ba3"}
                      strokeWidth={10}/>
        </>
    );
};

export default ShotsTaken;

import React, {useRef} from 'react';
import {Col, Row} from "antd"

import {TimelineItem} from "../types/types";

import FeedItem from "./FeedItem"
import connection, {roomTime, useTimeline} from "../lib/Connection";
import {useUpdateAfterDelay} from "../util/hooks";
import CSSTransition from "react-transition-group/CSSTransition";
import TransitionGroup from "react-transition-group/TransitionGroup";
import NextShot from "./NextShot";
import ShotsTaken from "./ShotsTaken";
import Player from "./Player";

import '../css/feed.sass'
import FeedTicker from "./FeedTicker";


const Feed = (props: any) => {
    const timeline = useTimeline();

    const feedElement = useRef<HTMLDivElement>(null);

    const lastShot = useRef<any>(null);

    useUpdateAfterDelay(getNextItemDelay())

    function getNextItemDelay() {
        if (!timeline) return 500;

        let [_, nextItem] = timeline.itemAtTime(roomTime());
        if (nextItem) {
            let nextItemTime = (nextItem.timestamp * 1000 - roomTime()) / (connection.room.get()?.speedFactor || 1)
            return nextItemTime;
        }
        return 500;
    }

    let liveFeed: TimelineItem[] = [];

    let shake = false;
    if (timeline != null) {
        liveFeed = timeline.feed.filter(item => {
            return item.timestamp * 1000 <= roomTime()
        });

        let lastEvent = timeline.eventAtTime(roomTime(), 'shot')[0];
        if (lastShot.current != lastEvent?.id) {
            lastShot.current = lastEvent?.id;
            shake = true;
        }
    }

    function doSingleShake() {
        if (!feedElement.current) return;

        if (feedElement.current.getAnimations().length > 0) return;

        if (feedElement.current.classList.contains('feed-shot-shake')) {
            feedElement.current.getAnimations().forEach((i) => i.finish());
            feedElement.current.classList.remove('feed-shot-shake');
            setTimeout(doSingleShake, 0);
            return;
        }

        feedElement.current.classList.add('feed-shot-shake');
        let el = feedElement.current;
        const listener = function () {
            el.classList.remove('feed-shot-shake');
            el.removeEventListener('animationend', listener);
        };
        el.addEventListener('animationend', listener);
    }

    if (shake && feedElement.current) {
        // Let the browser first do the heavy dom stuff to avoid lagging our
        // animation.
        setTimeout(doSingleShake, 100);
    }

    function itemOpacity(index: number) {
        // This is a bit of a hack but works better than trying to do this in css.
        // This fades out the elements the lower the element is on the screen.
        let itemHeight = document.querySelector('.feed-item-container')?.clientHeight;
        if (!itemHeight) itemHeight = 75;

        let totalItemsOnScreen = document.body.clientHeight / itemHeight;
        let visualIndex = liveFeed.length - 1 - index;

        let x = visualIndex / (totalItemsOnScreen - 1.8);
        x = Math.min(1, Math.max(0, x));
        return (1 - Math.pow(x, 4));
    }

    const debug = true;
    if (debug) {
        // @ts-ignore
        if (!window['__feedShakeDebug']) {
            // @ts-ignore
            window['__feedShakeDebug'] = true;
            window.document.documentElement.addEventListener('keydown', (e) => {
                if (e.keyCode == 67) { // c
                    doSingleShake();
                }
            })
        }
    }

    return (
        <div className="feed" ref={feedElement}>
            <Row className="feed-items">
                <Col span={24} md={4} className="sider">
                    <NextShot/>
                    <Player/>
                </Col>
                <Col span={24} md={16}>
                    <TransitionGroup className="feed-reverse">
                        {liveFeed.map((item, i) =>
                            item.events.map((event, j) =>
                                <CSSTransition
                                    timeout={300}
                                    classNames="fade"
                                    key={`${item.timestamp}.${j}`}>

                                    <div
                                        className="feed-item-container"
                                        style={{opacity: itemOpacity(i)}}>

                                        <FeedItem
                                            item={event}
                                            key={`${item.timestamp}.${j}f`}
                                        />

                                    </div>
                                </CSSTransition>
                            )
                        )}
                    </TransitionGroup>
                </Col>
                <Col span={24} md={4} className="sider">
                    <ShotsTaken/>
                </Col>
            </Row>
            <Row className="ticker">
                <FeedTicker />
            </Row>
        </div>
    );
};

export default Feed;

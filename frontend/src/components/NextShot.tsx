import React from 'react';
import {Col, Progress} from "antd"
import {roomTime, useTimeline} from "../lib/Connection";
import {useUpdateAfterDelay} from "../util/hooks";


const NextShot = () => {
    const timeline = useTimeline()

    useUpdateAfterDelay(1000)

    let remainingTime = 0;
    let remainingPercentage = 0;

    if (timeline) {
        const time = roomTime();
        const [current, next] = timeline.itemAtTime(time, 'shot');

        if (current && next) {
            let currentTime = time - current.timestamp * 1000
            let nextTime = next.timestamp * 1000 - current.timestamp * 1000;

            remainingTime = Math.round((nextTime - currentTime) / 1000)
            remainingPercentage = 100 - (currentTime / (nextTime || 1)) * 100;
        }
    }

    return (
        <>
            <h1>Tijd tot volgende shot:</h1>
            <Progress type="circle"
                      percent={remainingPercentage}
                      format={_ => remainingTime + ' sec.'}
                      strokeColor={"#304ba3"}
                      strokeWidth={10}
                      status="normal"/>
        </>
    );
};

export default NextShot;

import React from "react";

import {useRoomRunningAndReadyChanged} from "../lib/Connection";
import Feed from "./Feed";
import Lobby from "./Lobby";


const Centurion = () => {
    const room = useRoomRunningAndReadyChanged();
    const showFeed = (room?.running && room.readyToParticipate) || false;

    const feedContent = (
        <React.Fragment>
            <Feed/>
        </React.Fragment>
    );

    const lobbyContent = (
        <Lobby/>
    );

    return (
        <>
            <section className="content">
                {showFeed ? feedContent : lobbyContent}
            </section>
        </>
    );
};

export default Centurion;
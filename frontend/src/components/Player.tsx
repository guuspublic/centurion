import {roomTime, useRoomRunningAndReadyChanged, useRoomTime, useTimelineSongFileChanged} from "../lib/Connection";
import React, {ChangeEvent, createRef, SyntheticEvent, useRef, useState} from "react";

import {Button, Slider} from 'antd'
import {SoundFilled, SoundOutlined} from '@ant-design/icons';

import '../css/player.sass'
import {Room} from "../types/types";
import {parse as parseQueryString} from "query-string";

const Player = () => {
    const room = useRoomRunningAndReadyChanged();
    const _ = useRoomTime()
    const timeline = useTimelineSongFileChanged();

    let player = useRef<HTMLAudioElement>(null);
    const [volume, setVolume] = useState(100);
    const [muted, setMuted] = useState(false);

    const [timesSeeked, setTimesSeeked] = useState(0);
    const [hadError, setHadError] = useState(false);

    // If our time synchronisation algorithm thing thinks the time is off by more
    // than this value, we seek the running player to correct it.
    const diffSecondsRequiredToSeekRunningPlayer = 0.20;

    // Hard cap we are allowed to seek this player. Some browsers are slow or inaccurate
    // and will always be off. To avoid endless skipping of the song this cap stops seeking the
    // player.
    const maxTimesSeekAllow = 25;

    const query = parseQueryString(window.location.search);
    if (query.nosound) {
        return null;
    }

    if (player.current && player.current.dataset.src != timeline!!.songFile) {
        player.current.dataset.src = timeline!!.songFile;
        player.current.src = timeline!!.songFile;
    }

    function handlePlayerOnPlay(e: SyntheticEvent) {
        e.preventDefault();

        // For when the user manually started the player for when autoplay is off.
        setHadError(false);
        if (shouldPlay()) {
            startPlaying(true)
        }
    }

    function shouldPlay() {
        return player.current && timeline && room && room.running && room.readyToParticipate
    }

    function startPlaying(manual: boolean) {
        if (!player.current) return;

        if (player.current.paused && !hadError) {
            player.current.play().then(() => {
                setHadError(false);
            }).catch(e => {
                console.error('Error playing', e);
                setHadError(true);
            })
        }

        if (!hadError) {
            setPlayerTime(room!!, manual);
        }
    }

    function setPlayerTime(room: Room, manualAdjustment: boolean) {
        if (!player.current) return;

        let targetTime = roomTime() / 1000;
        let diff = player.current.currentTime - targetTime;

        // console.log('PLAYER DIFF', diff,
        //     'min req to seek: ', diffSecondsRequiredToSeekRunningPlayer);

        if (player.current && Math.abs(diff) > diffSecondsRequiredToSeekRunningPlayer) {
            if (room.speedFactor != 1 || manualAdjustment || timesSeeked < maxTimesSeekAllow) {
                player.current.currentTime = targetTime;
                player.current.playbackRate = Math.max(Math.min(4.0, room.speedFactor), 0.25);

                if (!manualAdjustment) {
                    setTimesSeeked(timesSeeked + 1);
                }
                console.log('PLAYER SEEKED', 'Player was seeked (total: ' + timesSeeked + ')');
            } else {
                console.warn('The running player is off, but we\'ve changed the time ' +
                    'too often, skipping synchronizing the player.');
            }
        }
    }

    function toggleMute(){
        if (player.current != null){
            player.current.muted = !player.current.muted;
            setMuted(!muted);
        }
    }

    function changeVolume(newValue: number ) {
        setVolume(newValue);
        if (player.current){
            player.current.volume = newValue / 100;
        }
    };

    if (shouldPlay()) {
        startPlaying(false)
    } else {
        if (player.current) {
            player.current.pause();
        }
    }



    function render() {
        return (
            <div id="audio">
                <audio ref={player} className='player' controls={true} hidden={!hadError} onPlay={handlePlayerOnPlay}/>
                <div id="volume-control">

                    <Button onClick={toggleMute} shape="circle">
                        {muted ? <SoundOutlined /> : <SoundFilled />}
                    </Button>
                    <Slider
                        id="volume-slider"
                        defaultValue={volume}
                        onChange={changeVolume}
                        />
                </div>
            </div>
        )
    }

    return render();
}

export default Player;

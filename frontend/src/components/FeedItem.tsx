import React, {PureComponent} from 'react';

import {TimestampEvent} from "../types/types";

import '../css/feed.sass'
import shot from "../img/shot.png";
import song from "../img/song.png";
import talk from "../img/talk.png";
import time from "../img/time.png";

const images = {
    shot, song, talk, time
};

class FeedItem extends PureComponent<{item: TimestampEvent}> {
    render() {
        return (
            <div className="feed-item">
                <div className="feed-item__title">
                    {this.props.item.text[0]}
                </div>
                <div className="feed-item__emoji">
                    <img src={images[this.props.item.type]}/>
                </div>
                <div className="feed-item__desc">
                    {this.props.item.text[1]}
                </div>
            </div>
        );
    }
}

export default FeedItem;

import {Socket} from "socket.io";

import User from "./User";
import {getIndex, getNextShot, getTimeline, getTimelineNames, indexForTime} from "./timeline";
import {getCurrentTime} from "./util";

export interface RoomOptions {
    seekTime: number
    timelineName: string
}

interface TickerMessage {
    user: User,
    message: string
}

export default class Room {
    id: number = 0;
    users: User[] = [];
    leader: User | null = null;

    ticker: TickerMessage[] = [];

    running = false;
    startTime = 0;

    seekTime: number = 0;
    timelineName: string = 'Centurion';

    // For debugging purposes
    speedFactor = 1;
    autoStart = false;

    constructor(name: number) {
        this.id = name;
    }

    serialize(user: User) {
        return {
            'id': this.id,
            'userCount': this.users.length,
            'isLeader': this.leader == user,
            'running': this.running,
            'startTime': this.startTime,
            'timelineName': this.timelineName,
            'seekTime': this.seekTime,
            'readyToParticipate': user.readyToParticipate || this.leader == user,
            'speedFactor': this.speedFactor,
            'ticker': this.ticker.map(i => i.message),
        }
    }

    serializeTimeline(user: User) {
        return getTimeline(this.timelineName);
    }

    sync() {
        this.users.forEach(u => u.sync());
    }

    join(user: User) {
        this.users.push(user);
        user.setRoom(this);

        if (!this.hasLeader()) {
            this.setLeader(user);
        }

        if (this.autoStart) {
            this.seekTime = 1450000 / this.speedFactor;
            this.running = true;
            this.start();
        }

        this.sync();
    }

    leave(user: User) {
        this.users.splice(this.users.indexOf(user), 1);
        user.setRoom(null);

        if (this.leader == user) {
            this.setRandomLeader();
        }

        this.sync();
    }

    onBeforeDelete() {
    }

    setOptions(options: any) {
        this.seekTime = Math.max(0, Math.min(options.seekTime, 250 * 60 * 1000))
        if (getTimelineNames().indexOf(options.timelineName) >= 0) {
            this.timelineName = options.timelineName;
        }
        this.sync()
    }

    start() {
        this.running = true;
        this.startTime = getCurrentTime() - this.seekTime

        this.sync();
    }

    /**
     *
     * @returns {boolean}
     */
    hasUsers() {
        return this.users.length !== 0;
    }

    setRandomLeader() {
        if (this.hasUsers()) {
            this.leader = this.users[0];
        }
    }

    hasLeader(): boolean {
        return this.leader != null;
    }

    setLeader(user: User) {
        this.leader = user;
    }

    getLeader(): User | null {
        return this.leader;
    }

    submitTickerMessage(user: User, message: string) {
        message = message.replace('\n', '');

        let existing = -1;
        for (let i = 0; i < this.ticker.length; i++) {
            if (this.ticker[i].user === user) {
                existing = i;
                break;
            }
        }
        if (existing >= 0) {
            this.ticker.splice(existing, 1);
        }

        this.ticker.push({
            user: user,
            message: message
        });

        this.sync();
    }
};

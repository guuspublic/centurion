import {Socket} from "socket.io";

import User from './User'
import Room, {RoomOptions} from './Room'
import {getCurrentTime, randomInt} from "./util";

export default class Service {
    private roomIdToRooms = new Map<number, Room>();
    private socketsToUsers = new Map<string, User>();

    onSocketConnect(socket: Socket) {
        let user = new User(socket);
        this.socketsToUsers.set(socket.id, user);
        user.sync();
    }

    onSocketDisconnect(socket: Socket) {
        let user = this.getUser(socket);

        if (user.room != null) {
            user.room.leave(user);
        }

        this.deleteEmptyRooms();

        user.onDisconnect();
    }

    onTimeSync(socket: Socket, requestId: number, clientTime: number) {
        let user = this.getUser(socket);

        let now = getCurrentTime();
        user.emit('time_sync', {
            'requestId': requestId,
            'clientDiff': now - clientTime,
            'serverTime': now
        })
    }

    onSetRoomOptions(socket: Socket, options: RoomOptions) {
        let user = this.getUser(socket);

        if (user.room?.getLeader() == user) {
            user.room!!.setOptions(options)
        }
    }

    onRequestStart(socket: Socket) {
        let user = this.getUser(socket);

        if (user.room?.getLeader() == user) {
            user.room!!.start();
        }
    }

    onRequestJoin(socket: Socket, roomId: number): boolean {
        let user = this.getUser(socket);
        if (user.room && user.room.id == roomId) return false;

        if (user.room) {
            user.room.leave(user);
            this.deleteEmptyRooms();
        }

        if (!this.roomIdToRooms.has(roomId)) {
            this.createRoomWithId(roomId);
        }

        let room = this.roomIdToRooms.get(roomId)!!;
        room.join(user);

        return true;
    }

    onRequestReady(socket: Socket) {
        let user = this.getUser(socket);
        if (!user.room || user.readyToParticipate) return;
        user.readyToParticipate = true;
        user.sync();
    }

    onRequestJoinRandom(socket: Socket) {
        let user = this.getUser(socket);

        if (user.room) {
            user.room.leave(user);
            this.deleteEmptyRooms();
        }

        const room = this.createRandomRoom();
        if (!room) throw Error('Too many rooms active');
        room.join(user);
    }

    hasRoomId(roomId: number): boolean {
        return this.roomIdToRooms.has(roomId);
    }

    submitTickerMessage(socket: Socket, message: string) {
        let user = this.getUser(socket);

        if (!user.room) {
            throw new Error('User has no room');
        }

        user.room.submitTickerMessage(user, message);
    }

    private getUser(socket: Socket): User {
        let user = this.socketsToUsers.get(socket.id);
        if (!user) {
            throw new Error('User not found');
        }
        return user;
    }

    private deleteEmptyRooms() {
        for (let room of this.roomIdToRooms.values()) {
            if (room.users.length == 0) {
                this.deleteRoom(room);
            }
        }
    }

    private createRandomRoom(): Room | null {
        let tries = 0;
        let i = 1;
        while (tries++ < 1000) {
            if (this.roomIdToRooms.has(i)) {
                i++;
            } else {
                return this.createRoomWithId(i);
            }
        }
        return null;
    }

    private createRoomWithId(roomId: number): Room {
        if (this.roomIdToRooms.has(roomId)) {
            throw new Error('A room with the given id already exists');
        }

        let room = new Room(roomId);
        this.roomIdToRooms.set(roomId, room);
        return room;
    }

    private deleteRoom(room: Room) {
        this.roomIdToRooms.get(room.id)!!.onBeforeDelete();
        this.roomIdToRooms.delete(room.id)
    }
}

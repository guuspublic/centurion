import express from "express";
import SocketIO, {Socket} from "socket.io";
import path from "path";

import Service from './Service'

// process.on('SIGINT', () => process.exit());
// process.on('SIGTERM', () => process.exit());

const HOST = '0.0.0.0';
const PORT = 3001;

const app = express();
const server = app.listen(PORT, HOST, () => console.log(`Centurion listening on port ${PORT}!`));
app.use(express.static(path.join(__dirname, '../public')));

const io = SocketIO(server);

const service = new Service();

io.on('connection', socket => {
    socket.on('disconnect', (reason) => {
        service.onSocketDisconnect(socket);
    });

    socket.on('ping', () => {
        socket.emit('pong');
    })

    socket.on('time_sync', (requestId: number, clientTime: number) => {
        if (!Number.isSafeInteger(requestId)) return;
        if (!Number.isSafeInteger(clientTime)) return;

        service.onTimeSync(socket, requestId, clientTime);
    })

    socket.on('room_options', (options) => {
        if (!options) return;
        if (!options.timelineName || typeof (options.timelineName) !== 'string') return;
        if (!Number.isSafeInteger(options.seekTime)) return;

        service.onSetRoomOptions(socket, options);
    });

    socket.on('request_start', (options) => {
        service.onRequestStart(socket);
    });

    socket.on('request_join', (roomId: number) => {
        if (!Number.isSafeInteger(roomId)) return;

        service.onRequestJoin(socket, roomId);
    });

    socket.on('request_ready', () => {
        service.onRequestReady(socket);
    })

    socket.on('request_join_random', () => {
        service.onRequestJoinRandom(socket);
    })

    socket.on('call', (id: number, name: string, params: any) => {
        if (!Number.isSafeInteger(id)) return;
        // noinspection SuspiciousTypeOfGuard
        if (!name || typeof (name) !== 'string') return;
        // if (!params) return;

        let call = new Call(socket, id, name, params);

        if (name == 'room_exists') {
            let roomId = params && params['roomId'];
            if (!Number.isSafeInteger(roomId)) {
                call.error('Invalid room id');
                return;
            }

            call.respond(service.hasRoomId(roomId));
            return;
        } else if (name == 'submit_ticker_message') {
            let message = params && params['message'];
            if (typeof (message) !== 'string' || !message.length) {
                call.error('Invalid message');
                return;
            }
            if (message.length > 192) { // perfect voor het Wilhelmus
                call.error('Message too long');
                return;
            }

            try {
                call.respond(service.submitTickerMessage(socket, message));
            } catch (e) {
                console.error(e);
                call.error(e.message);
            }
            return;
        }
        //
        // if (name == 'request_join') {
        //     let roomId = params && params['roomId'];
        //     if (!Number.isSafeInteger(roomId)) {
        //         call.error('Invalid room id');
        //         return;
        //     }
        //     if (!service.hasRoomId(roomId)) {
        //         call.respond(false);
        //         return;
        //     }
        //     if (service.onRequestJoin(socket, roomId)) {
        //         call.respond(true);
        //     } else {
        //         call.respond(false);
        //     }
        // }
    })

    service.onSocketConnect(socket);

    /*socket.on('join_room', (roomId, callback) => {
        if (!callback || typeof callback !== 'function') {
            console.error("Join: Callback not a function.");
            return
        }

        if (!roomId) {
            return callback('no_room_id_given');
        }

        if (!Number.isSafeInteger(+roomId)) {
            return callback('room_id_not_integer');
        }

        console.log(`${socketId} wants to join '${roomId}'.`);

        // Leave current room first

        let currentRoom = service.getUserRoom(socketId);
        if (currentRoom) {
            socket.leave(currentRoom.name);
            service.leaveRoom(socketId);
        }

        const room = service.joinRoom(socketId, roomId);

        socket.join(room.name);
        sendRoom(socket, room);
    });

    socket.on('room_info', callback => {
        if (!callback || typeof callback !== 'function') {
            console.error("Room info: Callback not a function.");
            return
        }

        const room = service.getUserRoom(socketId);
        sendRoom(socket, room);
    });

    socket.on('request_start', (time = null) => {
        console.log('request start', socket.rooms);

        const room = service.getUserRoom(socketId);
        if (!room.isLeader(socketId)) {
            console.warn("Non leader tried to start.");
            return;
        }

        room.run(io);
        sendRoom(io.to(room.name.toString()), room);

        if (typeof time === 'number' && time) {
            console.log("Starting at", time);
            room.seek(io, time);
        }
    });*/
});

class Call {
    private socket: Socket;
    private id: number;
    private name: string;
    private params: any;

    constructor(socket: Socket, id: number, name: string, params: any) {
        this.socket = socket;
        this.id = id;
        this.name = name;
        this.params = params;
    }

    error(reason: string) {
        this.socket.emit('call_response', {
            'id': this.id,
            'error': reason
        })
    }

    respond(data: any) {
        this.socket.emit('call_response', {
            'id': this.id,
            'response': data
        });
    }
}

// @ts-ignore
import timeline from '../data/timelines.js';


export function getTimelineNames(): string[] {
    return timeline.timelines.map((i: any) => i.name)
}

export function getTimeline(name: string) {
    let t = timeline.timelines.find((i: any) => i.name == name);
    if (!t) return null;
    return t;
}

/**
 *
 * @param i
 * @returns {*}
 */
export function getIndex(i: number): any {
    if (i >= timeline.length) {
        return;
    }

    return timeline[i];
}

/**
 * @param {number} i - the index.
 * @returns {{count: number, timestamp: number}|undefined}
 */
export function getNextShot(i: number) {
    for (; i < timeline.length; i++) {
        const time = getIndex(i);

        for (let event of time.events) {
            if (event.type === 'shot') {
                return {
                    timestamp: time.timestamp,
                    count: event.shotCount
                }
            }
        }
    }

    return undefined;
}

export function indexForTime(seconds: number): number {
    let lastIndex = 0;
    for (let i = 0; i < timeline.length; i++) {
        const time = timeline[i];

        if (time.timestamp >= seconds) {
            return lastIndex;
        }

        lastIndex = i;
    }

    return -1;
}

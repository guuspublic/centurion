/**
 * Generates random int
 * @param {number} min, inclusive
 * @param {number} max, exclusive
 * @returns {number}
 */
export function randomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

let _randomTimeOffsetForDebug = randomInt(-10000, 10000);
_randomTimeOffsetForDebug = 0;

console.log('random time offset', _randomTimeOffsetForDebug);

export function getCurrentTime() {
    return Date.now() + _randomTimeOffsetForDebug;
}

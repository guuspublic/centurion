module.exports = {
    'timelines': [
        {
            'name': 'Centurion',
            'songFile': 'songs/centurion.m4a',
            'feed': [

                {
                    "timestamp": 0,
                    "events": [
                        {
                            "type": "talk",
                            "text": [
                                "Zet je schrap!",
                                "We gaan zo beginnen"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 42,
                    "events": [
                        {
                            "type": "talk",
                            "text": [
                                "Daar gaan we!",
                                "Dit was het startsein, je hoeft nog niet te drinken"
                            ]
                        },
                        {
                            "type": "song",
                            "text": [
                                "Nena",
                                "99 Luftballons"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 108,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "De eerste!",
                                "Nog 99 shotjes"
                            ],
                            "shotCount": 1
                        }
                    ]
                },
                {
                    "timestamp": 148,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Hermes House Band",
                                "Country Roads"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 167,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Nummertje twee!",
                                "Nog 98 shotjes"
                            ],
                            "shotCount": 2
                        }
                    ]
                },
                {
                    "timestamp": 185,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Vinzzent",
                                "Dromendans"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 223,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 97 shotjes"
                            ],
                            "shotCount": 3
                        },
                        {
                            "type": "song",
                            "text": [
                                "Linda, Roos & Jessica",
                                "Ademnood"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 283,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Lustrum!",
                                "Nog 96 shotjes"
                            ],
                            "shotCount": 4
                        },
                        {
                            "type": "song",
                            "text": [
                                "Peter de Koning",
                                "Het is altijd lente in de ogen van de tandarts-assistente"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 340,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Niet gooien!",
                                "Nog 95 shotjes"
                            ],
                            "shotCount": 5
                        },
                        {
                            "type": "song",
                            "text": [
                                "Liquido",
                                "Narcotic"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 412,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 94 shotjes"
                            ],
                            "shotCount": 6
                        },
                        {
                            "type": "song",
                            "text": [
                                "Snoop Dogg feat. Pharrell",
                                "Drop It Like It's Hot"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 449,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "M.O.P.",
                                "Ante Up"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 459,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 93 shotjes"
                            ],
                            "shotCount": 7
                        }
                    ]
                },
                {
                    "timestamp": 514,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Trek een ad!",
                                "Nog 92 shotjes"
                            ],
                            "shotCount": 8
                        },
                        {
                            "type": "song",
                            "text": [
                                "Los Del Rio",
                                "Macarena"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 560,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Spice Girls",
                                "Wannabe"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 574,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "We zijn er nog lang niet!",
                                "Nog 91 shotjes"
                            ],
                            "shotCount": 9
                        }
                    ]
                },
                {
                    "timestamp": 617,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Major Lazer feat. Busy Signal, The Flexican & FS Green",
                                "Watch Out For This (Bumaye)"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 635,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Nummer tien!",
                                "Nog 90 shotjes"
                            ],
                            "shotCount": 10
                        }
                    ]
                },
                {
                    "timestamp": 647,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 90 minuten!",
                                "Geef alles, behalve op"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 684,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Kabouter Plop",
                                "Kabouterdans"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 699,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Met vriendelijke toet!",
                                "Nog 89 shotjes"
                            ],
                            "shotCount": 11
                        }
                    ]
                },
                {
                    "timestamp": 725,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "K3",
                                "Alle kleuren"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 756,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 88 shotjes"
                            ],
                            "shotCount": 12
                        }
                    ]
                },
                {
                    "timestamp": 757,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Kinderen voor Kinderen",
                                "Tietenlied"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 812,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Ongeluksshotje 13!",
                                "Nog 87 shotjes"
                            ],
                            "shotCount": 13
                        }
                    ]
                },
                {
                    "timestamp": 814,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Guus Meeuwis",
                                "Het dondert en het bliksemt"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 820,
                    "events": [
                        {
                            "type": "talk",
                            "text": [
                                "Carnaval is prachtig!",
                                "Leve de vervoerie"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 874,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 86 shotjes"
                            ],
                            "shotCount": 14
                        }
                    ]
                },
                {
                    "timestamp": 876,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Harry Vermeegen",
                                "1-2-3-4 Dennis bier"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 906,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Puhdys",
                                "Hey, wir woll’n die Eisbär'n sehn!"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 935,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Fünfzehn!",
                                "Nog 85 shotjes"
                            ],
                            "shotCount": 15
                        }
                    ]
                },
                {
                    "timestamp": 966,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "DJ Ötzi",
                                "Burger Dance"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 995,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toet!",
                                "Nog 84 shotjes"
                            ],
                            "shotCount": 16
                        }
                    ]
                },
                {
                    "timestamp": 996,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Mickie Krause",
                                "Hütte auf der Alm"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1030,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Ali B & Yes-R & The Partysquad",
                                "Rampeneren"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1046,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Zuipen!",
                                "Nog 83 shotjes"
                            ],
                            "shotCount": 17
                        }
                    ]
                },
                {
                    "timestamp": 1107,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Met dank aan bestuur 19; super 'vo",
                                "Nog 82 shotjes"
                            ],
                            "shotCount": 18
                        },
                        {
                            "type": "song",
                            "text": [
                                "Martin Solveig",
                                "Intoxicated"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1137,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Nicki Minaj",
                                "Starships"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1173,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Laatste shotje als tiener!",
                                "Nog 81 shotjes"
                            ],
                            "shotCount": 19
                        }
                    ]
                },
                {
                    "timestamp": 1222,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "2Unlimited",
                                "Get Ready For This"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1233,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 80 minuten!",
                                "Al 19 shotjes achter de rug"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1235,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Adje!",
                                "Nog 80 shotjes"
                            ],
                            "shotCount": 20
                        }
                    ]
                },
                {
                    "timestamp": 1275,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "The Village People",
                                "YMCA"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1288,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 79 shotjes"
                            ],
                            "shotCount": 21
                        }
                    ]
                },
                {
                    "timestamp": 1348,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 78 shotjes"
                            ],
                            "shotCount": 22
                        }
                    ]
                },
                {
                    "timestamp": 1350,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Carly Rae Jepsen ft Owl City",
                                "It's Always A Good Time"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1395,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Avicii",
                                "Levels"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1411,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Claxon!",
                                "Nog 77 shotjes"
                            ],
                            "shotCount": 23
                        }
                    ]
                },
                {
                    "timestamp": 1456,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Flo-Rida feat. T-Pain",
                                "Low"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1471,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "En nu even op standje maximaal!",
                                "Nog 76 shotjes"
                            ],
                            "shotCount": 24
                        }
                    ]
                },
                {
                    "timestamp": 1486,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Taio Cruz",
                                "Hangover"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1522,
                    "events": [
                        {
                            "type": "talk",
                            "text": [
                                "Dit is nog het rustige stukje!",
                                "Zet hem maar op de bonk-bonk"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1530,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Halve Abraham!",
                                "Nog 75 shotjes"
                            ],
                            "shotCount": 25
                        }
                    ]
                },
                {
                    "timestamp": 1545,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "LMFAO",
                                "Party Rock Anthem"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1594,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Hoch die Hände!",
                                "Nog 74 shotjes"
                            ],
                            "shotCount": 26
                        },
                        {
                            "type": "song",
                            "text": [
                                "Hans Entertainment vs. Finger & Kadel",
                                "Hoch die Hände"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1623,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Galantis",
                                "No Money"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1653,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Voel je 'm al?",
                                "Nog 73 shotjes"
                            ],
                            "shotCount": 27
                        }
                    ]
                },
                {
                    "timestamp": 1683,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Kid Cudi",
                                "Pursuit of Happiness (Steve Aoki remix)"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1712,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Project X!",
                                "Nog 72 shotjes"
                            ],
                            "shotCount": 28
                        }
                    ]
                },
                {
                    "timestamp": 1741,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Yeah Yeah Yeahs",
                                "Heads Will Roll (A-Trak remix)"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1769,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 71 shotjes"
                            ],
                            "shotCount": 29
                        }
                    ]
                },
                {
                    "timestamp": 1814,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Michael Calfan",
                                "Resurrection"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1825,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 70 minuten!",
                                "Half uurtje zit erop"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1829,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toet!",
                                "Nog 70 shotjes"
                            ],
                            "shotCount": 30
                        }
                    ]
                },
                {
                    "timestamp": 1858,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Basto!",
                                "Again and Again"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1887,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Over de dertig!",
                                "Nog 69 (hehe) shotjes"
                            ],
                            "shotCount": 31
                        }
                    ]
                },
                {
                    "timestamp": 1916,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "David Guetaa feat. Sia",
                                "Titanium"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 1945,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Trek een ad!",
                                "Nog 68 shotjes"
                            ],
                            "shotCount": 32
                        }
                    ]
                },
                {
                    "timestamp": 1959,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Gala",
                                "Freed From Desire"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2004,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Nummertje 33!",
                                "Nog 67 shotjes!"
                            ],
                            "shotCount": 33
                        }
                    ]
                },
                {
                    "timestamp": 2034,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Wolter Kroes",
                                "Viva Hollandia"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2064,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Voor het vaderland!",
                                "Nog 66 shotjes"
                            ],
                            "shotCount": 34
                        }
                    ]
                },
                {
                    "timestamp": 2125,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 65 shotjes"
                            ],
                            "shotCount": 35
                        }
                    ]
                },
                {
                    "timestamp": 2128,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Westlife",
                                "Uptown Girl"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2179,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Aqua",
                                "Barbie Girl"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2194,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 64 shotjes"
                            ],
                            "shotCount": 36
                        }
                    ]
                },
                {
                    "timestamp": 2225,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Guillerma & Tropical Danny",
                                "Toppertje"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2245,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "In dat keelgaatje!",
                                "Nog 63 shotjes"
                            ],
                            "shotCount": 37
                        }
                    ]
                },
                {
                    "timestamp": 2305,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 62 shotjes"
                            ],
                            "shotCount": 38
                        },
                        {
                            "type": "song",
                            "text": [
                                "The Bloody Beetroots feat. Steve Aoki",
                                "Warp 1.9"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2374,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toet!",
                                "Nog 61 shotjes"
                            ],
                            "shotCount": 39
                        },
                        {
                            "type": "song",
                            "text": [
                                "David Guetta & Showtek feat. Vassy",
                                "Bad"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2431,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "40 alweer!",
                                "Nog 60 shotjes"
                            ],
                            "shotCount": 40
                        },
                        {
                            "type": "song",
                            "text": [
                                "Showtek & Justin Prime",
                                "Cannonball"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2444,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 60 minuten!",
                                "Een klein uurtje"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2460,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Die Atzen",
                                "Disco Pogo"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2489,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Zuipen!",
                                "Nog 59 shotjes"
                            ],
                            "shotCount": 41
                        }
                    ]
                },
                {
                    "timestamp": 2534,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Lorenz Büffel",
                                "Johnny Däpp"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2555,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Dab dat glas naar je mond!",
                                "Nog 58 shotjes"
                            ],
                            "shotCount": 42
                        }
                    ]
                },
                {
                    "timestamp": 2587,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Zware Jongens",
                                "Jodeljump"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2606,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 57 shotjes"
                            ],
                            "shotCount": 43
                        }
                    ]
                },
                {
                    "timestamp": 2635,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Parla & Pardoux",
                                "Liberté"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2664,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 56 shotjes"
                            ],
                            "shotCount": 44
                        }
                    ]
                },
                {
                    "timestamp": 2695,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Markus Becker",
                                "Das rote Pferd"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2729,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 55 shotjes"
                            ],
                            "shotCount": 45
                        }
                    ]
                },
                {
                    "timestamp": 2743,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Olaf Henning",
                                "Cowboy und Indianer"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2784,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Adje!",
                                "Nog 54 shotjes"
                            ],
                            "shotCount": 46
                        }
                    ]
                },
                {
                    "timestamp": 2785,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Ch!pz",
                                "Cowboy"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2824,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Toy-Box",
                                "Tarzan & Jane"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2850,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 53 shotjes"
                            ],
                            "shotCount": 47
                        }
                    ]
                },
                {
                    "timestamp": 2879,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Toy-Box",
                                "Sailor Song"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2919,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Bijna op de helft!",
                                "Nog 52 shotjes"
                            ],
                            "shotCount": 48
                        }
                    ]
                },
                {
                    "timestamp": 2921,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Vengaboys",
                                "Boom, Boom, Boom, Boom!!"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 2975,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Bam!",
                                "Nog 51 shotjes"
                            ],
                            "shotCount": 49
                        }
                    ]
                },
                {
                    "timestamp": 2998,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Vengaboys",
                                "To Brazil!"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3024,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 50 minuten!",
                                "We zijn op de helft!"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3039,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Abraham!",
                                "Nog 50 shotjes"
                            ],
                            "shotCount": 50
                        }
                    ]
                },
                {
                    "timestamp": 3070,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Snollebollekes",
                                "Bam bam (bam)"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3104,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Tweede helft!",
                                "Nog 49 shotjes"
                            ],
                            "shotCount": 51
                        }
                    ]
                },
                {
                    "timestamp": 3120,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Def Rhymz",
                                "Schudden"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3160,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 48 shotjes"
                            ],
                            "shotCount": 52
                        }
                    ]
                },
                {
                    "timestamp": 3190,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Cooldown Café",
                                "Hey baby"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3215,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 47 shotjes"
                            ],
                            "shotCount": 53
                        }
                    ]
                },
                {
                    "timestamp": 3232,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Gebroeders Ko",
                                "Schatje, mag ik je foto"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3279,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Zuipen!",
                                "Nog 46 shotjes"
                            ],
                            "shotCount": 54
                        },
                        {
                            "type": "song",
                            "text": [
                                "Guus Meeuwis",
                                "Het is een nacht"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3341,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Adje!",
                                "Nog 45 shotjes"
                            ],
                            "shotCount": 55
                        }
                    ]
                },
                {
                    "timestamp": 3356,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Tom Waes",
                                "Dos cervezas"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3398,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 44 shotjes"
                            ],
                            "shotCount": 56
                        }
                    ]
                },
                {
                    "timestamp": 3412,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Peter Wackel",
                                "Vollgas"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3439,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Peter Wackel",
                                "Scheiß drauf!"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3465,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Hard gaan!",
                                "Nog 43 shotjes"
                            ],
                            "shotCount": 57
                        }
                    ]
                },
                {
                    "timestamp": 3467,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Ikke Hüftgold",
                                "Dicke titten, kartoffelsalat"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3520,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Hap, slok, weg!",
                                "Nog 42 shotjes"
                            ],
                            "shotCount": 58
                        }
                    ]
                },
                {
                    "timestamp": 3521,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Tim Toupet",
                                "Fliegerlied (So ein schöner Tag)"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3564,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Cooldown Café",
                                "Met z'n allen"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3577,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 41 shotjes"
                            ],
                            "shotCount": 59
                        }
                    ]
                },
                {
                    "timestamp": 3636,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 40 minuten!",
                                "Uurtje achter de rug"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3643,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Glas 60!",
                                "Nog 40 shotjes"
                            ],
                            "shotCount": 60
                        }
                    ]
                },
                {
                    "timestamp": 3659,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "The Partysquad feat. Jayh, Sjaak & Reverse",
                                "Helemaal naar de klote"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3687,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "K-Liber",
                                "Viben"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3700,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Adje!",
                                "Nog 39 shotjes"
                            ],
                            "shotCount": 61
                        }
                    ]
                },
                {
                    "timestamp": 3753,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 38 shotjes"
                            ],
                            "shotCount": 62
                        }
                    ]
                },
                {
                    "timestamp": 3755,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "FeestDJRuud & Dirtcaps feat. Sjaak & Kraantje Pappie",
                                "Weekend"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3808,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 37 shotjes"
                            ],
                            "shotCount": 63
                        },
                        {
                            "type": "song",
                            "text": [
                                "Lawineboys",
                                "Joost"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3860,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Gebroeders Ko",
                                "Ik heb een toeter op mijn waterscooter"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3874,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toe-toe-toeter!",
                                "Nog 36 shotjes"
                            ],
                            "shotCount": 64
                        }
                    ]
                },
                {
                    "timestamp": 3904,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Gebroeders Ko",
                                "Tringeling"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3931,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Tringeling!",
                                "Nog 35 shotjes"
                            ],
                            "shotCount": 65
                        }
                    ]
                },
                {
                    "timestamp": 3975,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Basshunter",
                                "Boten Anna"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 3988,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Zuipen!",
                                "Nog 34 shotjes"
                            ],
                            "shotCount": 66
                        }
                    ]
                },
                {
                    "timestamp": 4029,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Lawineboys",
                                "Wat zullen we drinken"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4050,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Dorst!",
                                "Nog 33 shotjes"
                            ],
                            "shotCount": 67
                        }
                    ]
                },
                {
                    "timestamp": 4081,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Lamme Frans",
                                "Wakker met een biertje!"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4113,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Biertje in de hand...",
                                "Nog 32 shotjes"
                            ],
                            "shotCount": 68
                        }
                    ]
                },
                {
                    "timestamp": 4124,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Lawineboys feat. DJ Jerome",
                                "Seks met die kale"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4175,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 31 shotjes"
                            ],
                            "shotCount": 69
                        }
                    ]
                },
                {
                    "timestamp": 4177,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Zombie Nation",
                                "Kernkraft 400"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4214,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 30 minuten!",
                                "Half uurtje nog maar"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4228,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 30 shotjes"
                            ],
                            "shotCount": 70
                        }
                    ]
                },
                {
                    "timestamp": 4230,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "DJ Boozywoozy",
                                "Party Affair"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4280,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Tik maar achterover!",
                                "Nog 29 shotjes"
                            ],
                            "shotCount": 71
                        }
                    ]
                },
                {
                    "timestamp": 4289,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "2Unlimited",
                                "No Limit"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4341,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Alcoholic party!",
                                "Nog 28 shotjes"
                            ],
                            "shotCount": 72
                        }
                    ]
                },
                {
                    "timestamp": 4343,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "DJ Kicken vs. MC-Q",
                                "Ain't No Party"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4408,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toet!",
                                "Nog 27 shotjes"
                            ],
                            "shotCount": 73
                        }
                    ]
                },
                {
                    "timestamp": 4411,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Jan Wayne",
                                "Becuase the Night"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4455,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Cascada",
                                "Everytime We Touch"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4467,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 26 shotjes"
                            ],
                            "shotCount": 74
                        }
                    ]
                },
                {
                    "timestamp": 4510,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Gigi D'Agostino",
                                "L'amour toujours"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4521,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Shotje 75!",
                                "Nog 25 shotjes"
                            ],
                            "shotCount": 75
                        }
                    ]
                },
                {
                    "timestamp": 4573,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Jason Paige",
                                "Gotta Catch 'M All"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4578,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Trek een ad!",
                                "Nog 24 shotjes"
                            ],
                            "shotCount": 76
                        }
                    ]
                },
                {
                    "timestamp": 4636,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Scooter",
                                "How Much Is The Fish"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4645,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 23 shotjes"
                            ],
                            "shotCount": 77
                        }
                    ]
                },
                {
                    "timestamp": 4675,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Scooter",
                                "Weekend"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4701,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "scToeter!",
                                "Nog 22 shotjes"
                            ],
                            "shotCount": 78
                        }
                    ]
                },
                {
                    "timestamp": 4714,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Scooter",
                                "One (Always Hardcore)"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4764,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "En door!",
                                "Nog 21 shotjes"
                            ],
                            "shotCount": 79
                        }
                    ]
                },
                {
                    "timestamp": 4766,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Scooter",
                                "Maria (I Like It Loud)"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4815,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 20 minuten!",
                                "Geef alles, behalve over"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4818,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Scooter",
                                "J'adore Hardcore"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4829,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Al 80 in de mik!",
                                "Nog 20 shotjes"
                            ],
                            "shotCount": 80
                        }
                    ]
                },
                {
                    "timestamp": 4860,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Wildstylez feat. Niels Geusebroek",
                                "Year of Summer"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4888,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 19 shotjes"
                            ],
                            "shotCount": 81
                        }
                    ]
                },
                {
                    "timestamp": 4929,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Brennan Heart & Wildstylez",
                                "Lose My Mind"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 4953,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Bakken vouwen!",
                                "Nog 18 shotjes"
                            ],
                            "shotCount": 82
                        }
                    ]
                },
                {
                    "timestamp": 5004,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 17 shotjes"
                            ],
                            "shotCount": 83
                        }
                    ]
                },
                {
                    "timestamp": 5006,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Starkoo",
                                "Ik wil je"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5059,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Feestteam",
                                "Let It Be"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5071,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Let it bier!",
                                "Nog 16 shotjes"
                            ],
                            "shotCount": 84
                        }
                    ]
                },
                {
                    "timestamp": 5125,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Adje numero 85!",
                                "Nog 15 shotjes"
                            ],
                            "shotCount": 85
                        }
                    ]
                },
                {
                    "timestamp": 5179,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "DJ Nikolai & DJ Mike van Dijk",
                                "Piano Man"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5188,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toet!",
                                "Nog 14 shotjes"
                            ],
                            "shotCount": 86
                        }
                    ]
                },
                {
                    "timestamp": 5190,
                    "events": [
                        {
                            "type": "talk",
                            "text": [
                                "Gewoon doorgaan!",
                                "Deze Piano Man telt niet"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5237,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Robbie Williams",
                                "Angels"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5243,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Drinken!",
                                "Nog 13 shotjes"
                            ],
                            "shotCount": 87
                        }
                    ]
                },
                {
                    "timestamp": 5278,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Enrique Iglesias",
                                "Hero"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5308,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "I can't be your biero!",
                                "Nog 12 shotjes"
                            ],
                            "shotCount": 88
                        }
                    ]
                },
                {
                    "timestamp": 5331,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Whitney Houston",
                                "I Will Always Love You"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5374,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 11 shotjes"
                            ],
                            "shotCount": 89
                        }
                    ]
                },
                {
                    "timestamp": 5380,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Mariah Carey",
                                "All I Want For Christmas"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5431,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "90 in de keel, hierna niet veel!",
                                "Nog 10 shotjes"
                            ],
                            "shotCount": 90
                        }
                    ]
                },
                {
                    "timestamp": 5441,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 10 minuten!",
                                "De laatste loodjes"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5444,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Kraantje Pappie",
                                "Feesttent (FeestDJRuud remix)"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5488,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Zuip je uit de dubbele cijfers!",
                                "Nog 9 shotjes"
                            ],
                            "shotCount": 91
                        }
                    ]
                },
                {
                    "timestamp": 5490,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "New Kids feat. DJ Paul Elstak",
                                "Turbo"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5556,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Zuipen!",
                                "Nog 8 shotjes"
                            ],
                            "shotCount": 92
                        }
                    ]
                },
                {
                    "timestamp": 5558,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Lipstick",
                                "I'm a Raver"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5591,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Nakatomi",
                                "Children of the Night"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5612,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Pweeeep!",
                                "Nog 7 shotjes"
                            ],
                            "shotCount": 93
                        }
                    ]
                },
                {
                    "timestamp": 5614,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Charly Lownoise & Mental Theo",
                                "Wonderful Days"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5659,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "DJ Paul Elstak",
                                "Luv You More"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5667,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Bijna daar!",
                                "Nog 6 shotjes"
                            ],
                            "shotCount": 94
                        }
                    ]
                },
                {
                    "timestamp": 5704,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "DJ Paul Elstak",
                                "Rainbow In The Sky"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5716,
                    "events": [
                        {
                            "type": "time",
                            "text": [
                                "Nog 5 minuten!",
                                "Zet de shotjes maar klaar"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5731,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Toeter!",
                                "Nog 5 shotjes"
                            ],
                            "shotCount": 95
                        }
                    ]
                },
                {
                    "timestamp": 5733,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Evil Activities",
                                "Nobody Said It Was Easy"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5783,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Slok 96!",
                                "Nog 4 shotjes"
                            ],
                            "shotCount": 96
                        }
                    ]
                },
                {
                    "timestamp": 5827,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Melrose",
                                "O"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5846,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Voor de 97e keer!",
                                "Nog 3 shotjes"
                            ],
                            "shotCount": 97
                        }
                    ]
                },
                {
                    "timestamp": 5855,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "Backstreet Boys",
                                "I Want It That Way"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5902,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Nummer 98!",
                                "Nog 2 shotjes"
                            ],
                            "shotCount": 98
                        }
                    ]
                },
                {
                    "timestamp": 5933,
                    "events": [
                        {
                            "type": "song",
                            "text": [
                                "R. Kelly",
                                "The World's Greatest"
                            ]
                        }
                    ]
                },
                {
                    "timestamp": 5970,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "Nummer 99!",
                                "Nog 1 shotje"
                            ],
                            "shotCount": 99
                        }
                    ]
                },
                {
                    "timestamp": 6020,
                    "events": [
                        {
                            "type": "shot",
                            "text": [
                                "CENTURION!",
                                "Geen shots meer"
                            ],
                            "shotCount": 100
                        }
                    ]
                }
            ]
        },
        {
            'name': 'Totale isolatie',
            'songFile': 'songs/totale_isolatie.m4a',
            'feed': [
                {
                    'timestamp': 0,
                    'events': [
                        {
                            'type': 'talk',
                            'text': [
                                'Ik heb geen tekst',
                                'want het is veel werk'
                            ]
                        }
                    ]
                },
                {
                    'timestamp': 10,
                    'events': [
                        {
                            'type': 'talk',
                            'text': [
                                'Nee echt',
                                'niks'
                            ]
                        },
                        {
                            'type': 'song',
                            'text': [
                                'Blahblah',
                                'blahblahblah'
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}
